package com.elciocestari.estoquedebebidas.service;

import com.elciocestari.estoquedebebidas.dto.BebidaDto;
import com.elciocestari.estoquedebebidas.dto.PerecivelDto;
import com.elciocestari.estoquedebebidas.dto.ProdutoDto;
import com.elciocestari.estoquedebebidas.entity.Produto;
import com.elciocestari.estoquedebebidas.exception.ResourceNotFoundException;
import com.elciocestari.estoquedebebidas.mapper.BebidaMapper;
import com.elciocestari.estoquedebebidas.mapper.PerecivelMapper;
import com.elciocestari.estoquedebebidas.mapper.ProdutoMapper;
import com.elciocestari.estoquedebebidas.repository.BebidaRepository;
import com.elciocestari.estoquedebebidas.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ProdutoService implements IProdutoService<ProdutoDto,Produto> {

    @Autowired private ProdutoRepository produtoRepository;
    @Autowired private BebidaRepository bebidaRepository;
    @Autowired private BebidaMapper bebidaMapper;
    @Autowired private BebidaService bebidaService;
    @Autowired private PerecivelMapper perecivelMapper;
    @Autowired private PerecivelService perecivelService;

    @Override
    public List<ProdutoDto> getAll() {
        List<? extends ProdutoDto> bebidaDtoList = this.bebidaService.getAll();
        List<? extends ProdutoDto> perecivelDtoList = this.perecivelService.getAll();
        List<ProdutoDto> produtoDtoList = Stream.concat(bebidaDtoList.stream(), perecivelDtoList.stream()).collect(Collectors.toList());
        return produtoDtoList;
    }

    @Override
    public ProdutoDto findById(Long id) {
        Optional<ProdutoDto> bebida = Optional.ofNullable(this.bebidaService.findById(id));
        if (bebida.isPresent()) return bebida.get();

        Optional<ProdutoDto> perecivel = Optional.ofNullable(this.perecivelService.findById(id));
        if (perecivel.isPresent()) return perecivel.get();

        throw new ResourceNotFoundException("Não foi encontrado um produto com id [ " + id + " ]");
    }

    @Override
    public ProdutoDto update(Long id, ProdutoDto entityDTO) {
        IProdutoService service = this.getServiceStrategy(entityDTO);
        return (ProdutoDto) service.update(id, entityDTO);
    }

    private Optional<ProdutoDto> getProdutoById(Long id, IProdutoService service, ProdutoMapper mapper) {
        ProdutoDto dto = (ProdutoDto) service.findById(id);
        return Optional.ofNullable(dto);
    }

    @Override
    public ProdutoDto save(ProdutoDto dto) {
        IProdutoService service = getServiceStrategy(dto);
        ProdutoDto produtoDto = (ProdutoDto) service.save(dto);
        return produtoDto;
    }

    private ProdutoMapper getMapperStrategy(ProdutoDto dto) {
        if (dto instanceof BebidaDto) return this.bebidaMapper;
        if (dto instanceof PerecivelDto) return this.perecivelMapper;

        throw new IllegalArgumentException("Não foi possivel realizar a conversão de " + dto);
    }

    private IProdutoService getServiceStrategy(ProdutoDto dto) {
        if (dto instanceof BebidaDto) return this.bebidaService;
        if (dto instanceof PerecivelDto) return this.perecivelService;
        return this;
    }
}
