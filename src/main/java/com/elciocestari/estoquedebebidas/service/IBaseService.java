package com.elciocestari.estoquedebebidas.service;

import com.elciocestari.estoquedebebidas.exception.ResourceNotFoundException;

import java.util.List;

public interface IBaseService<T, D> {

    List<T> getAll();

    T findById(Long id) throws ResourceNotFoundException;

    <S extends T> S save(S entityDTO);

    T update(Long id, T entityDTO);
}
