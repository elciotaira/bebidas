package com.elciocestari.estoquedebebidas.service;

import com.elciocestari.estoquedebebidas.dto.PerecivelDto;
import com.elciocestari.estoquedebebidas.entity.Perecivel;
import com.elciocestari.estoquedebebidas.exception.ResourceNotFoundException;
import com.elciocestari.estoquedebebidas.mapper.PerecivelMapper;
import com.elciocestari.estoquedebebidas.repository.PerecivelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PerecivelService implements IPerecivelService<PerecivelDto, Perecivel>{

    @Autowired private PerecivelRepository perecivelRepository;
    @Autowired private PerecivelMapper perecivelMapper;

    @Override
    public List<PerecivelDto> getAll() {
        List<Perecivel> perecivelList = this.perecivelRepository.findAll();
        return perecivelList.stream()
                .map(perecivel -> this.perecivelMapper.toDto(perecivel))
                .collect(Collectors.toList());
    }

    @Override
    public PerecivelDto findById(Long id) throws ResourceNotFoundException {
        Optional<Perecivel> perecivelOptional = this.perecivelRepository.findById(id);
        if (perecivelOptional.isPresent()) {
            return perecivelOptional.map( perecivel -> this.perecivelMapper.toDto(perecivel)).get();
        }
        return null;
    }

    @Override
    public PerecivelDto save(PerecivelDto entityDTO) {
        Perecivel perecivel = this.perecivelMapper.toModel(entityDTO);
        Optional<Perecivel> perecivelOptional = Optional.ofNullable(this.perecivelRepository.save(perecivel));
        if (perecivelOptional.isPresent()) {
            return this.perecivelMapper.toDto(perecivelOptional.get());
        }
        throw new RuntimeException("Erro ao salvar o Perecivel " + entityDTO);
    }

    @Override
    public PerecivelDto update(Long id, PerecivelDto entityDTO) {
        int updated = this.perecivelRepository.update(this.perecivelMapper.toModel(entityDTO), id);
        if (updated > 0) return entityDTO;
        throw new ResourceNotFoundException("Não foi encontrado um Perecivel com o id " + id);
    }
}
