package com.elciocestari.estoquedebebidas.service;

import com.elciocestari.estoquedebebidas.dto.BebidaDto;
import com.elciocestari.estoquedebebidas.entity.Bebida;
import com.elciocestari.estoquedebebidas.enums.TipoBebida;
import com.elciocestari.estoquedebebidas.exception.ResourceNotFoundException;
import com.elciocestari.estoquedebebidas.mapper.BebidaMapper;
import com.elciocestari.estoquedebebidas.repository.BebidaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BebidaService implements IBebidaService<BebidaDto, Bebida> {

    @Autowired BebidaRepository bebidaRepository;
    @Autowired BebidaMapper bebidaMapper;

    @Override
    public List<BebidaDto> getAll() {
        return this.bebidaRepository.findAll()
                .stream()
                .map((bebida) -> this.bebidaMapper.toDto(bebida))
                .collect(Collectors.toList());
    }

    @Override
    public BebidaDto findById(Long id) throws ResourceNotFoundException {
        Optional<Bebida> bebida = this.bebidaRepository.findById(id);
        if (bebida.isPresent()) {
            return this.bebidaMapper.toDto(bebida.get());
        }
        throw new ResourceNotFoundException("Não foi encontrado uma Bebida com o id " + id);
    }

    @Override
    public BebidaDto save(BebidaDto bebidaDto) {
        Bebida bebida = this.bebidaMapper.toModel(bebidaDto);
        Bebida bebidaSalva = this.bebidaRepository.save(bebida);
        return this.bebidaMapper.toDto(bebidaSalva);
    }

    @Override
    public BebidaDto update(Long id, BebidaDto entityDTO) {
        int updated = this.bebidaRepository.update(this.bebidaMapper.toModel(entityDTO),id);

        if (updated > 0) return entityDTO;
        throw new ResourceNotFoundException("Não foi encontrado uma bebida com o Id " + id);
    }

}
