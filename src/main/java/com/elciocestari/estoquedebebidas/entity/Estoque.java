package com.elciocestari.estoquedebebidas.entity;

import javax.persistence.Entity;

@Entity
public class Estoque extends BaseEntity {

    private double volumeTotal;

    public double getVolumeTotal() {
        return volumeTotal;
    }

    public void setVolumeTotal(double volumeTotal) {
        this.volumeTotal = volumeTotal;
    }
}
