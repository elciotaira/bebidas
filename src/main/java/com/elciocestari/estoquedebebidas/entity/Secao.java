package com.elciocestari.estoquedebebidas.entity;

import javax.persistence.Entity;

@Entity
public class Secao extends BaseEntity {

    private double quantidadeArmazenamento;

    public double getQuantidadeArmazenamento() {
        return quantidadeArmazenamento;
    }

    public void setQuantidadeArmazenamento(double quantidadeArmazenamento) {
        this.quantidadeArmazenamento = quantidadeArmazenamento;
    }
}
