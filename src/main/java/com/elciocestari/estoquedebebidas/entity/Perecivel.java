package com.elciocestari.estoquedebebidas.entity;

import lombok.experimental.SuperBuilder;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import lombok.Data;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
@Data
public class Perecivel extends Produto{
    private double temperadaEmGrausCelcius;
}



