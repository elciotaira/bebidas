package com.elciocestari.estoquedebebidas.entity;

import com.elciocestari.estoquedebebidas.enums.TipoBebida;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import java.util.Date;

@Data
@SuperBuilder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Bebida extends Produto {
    private TipoBebida tipoBebida;
//    private Date validade;
}
