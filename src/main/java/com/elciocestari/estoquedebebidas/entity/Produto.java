package com.elciocestari.estoquedebebidas.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.MappedSuperclass;
import java.util.Date;

@Data
@MappedSuperclass
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public abstract class Produto extends BaseEntity {

    private String nome;
    private double volume;
    private double quantidade;
    private Date validade;
}
