package com.elciocestari.estoquedebebidas.repository;

import com.elciocestari.estoquedebebidas.entity.Perecivel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface PerecivelRepository extends JpaRepository<Perecivel, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Perecivel p SET " +
            "p.temperadaEmGrausCelcius = :#{#p.temperadaEmGrausCelcius}, "+
            "p.nome = :#{#p.nome}, " +
            "p.volume = :#{#p.volume}, " +
            "p.quantidade = :#{#p.quantidade}, " +
            "p.validade = :#{#p.validade} " +
            "WHERE p.id = :id ")
    int update(@Param("p") Perecivel perecivel, Long id);
}
