package com.elciocestari.estoquedebebidas.repository;

import com.elciocestari.estoquedebebidas.entity.Bebida;
import com.elciocestari.estoquedebebidas.enums.TipoBebida;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;

@Repository
public interface BebidaRepository extends JpaRepository<Bebida, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Bebida b " +
            "SET " +
            "b.tipoBebida = :tipoBebida, " +
            "b.nome = :nome, " +
            "b.volume = :volume, " +
            "b.quantidade = :quantidade, " +
            "b.validade = :validade " +
            "WHERE b.id = :id")
    int update(TipoBebida tipoBebida, String nome, double volume, double quantidade, Date validade, Long id);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Bebida b " +
            "SET " +
            "b.tipoBebida = :#{#bebida.tipoBebida}, " +
            "b.nome = :#{#bebida.nome}, " +
            "b.volume = :#{#bebida.volume}, " +
            "b.quantidade = :#{#bebida.quantidade}, " +
            "b.validade = :#{#bebida.validade} " +
            "WHERE b.id = :id")
    int update(@Param("bebida") Bebida bebida, Long id);
}
