package com.elciocestari.estoquedebebidas.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class JsonParseException extends RuntimeException{
    public JsonParseException(String message) {
        super(message);
    }
}
