package com.elciocestari.estoquedebebidas.mapper;

import com.elciocestari.estoquedebebidas.dto.PerecivelDto;
import com.elciocestari.estoquedebebidas.entity.Perecivel;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PerecivelMapper extends ProdutoMapper<Perecivel, PerecivelDto>{

    PerecivelMapper INSTANCE = Mappers.getMapper(PerecivelMapper.class);

    Perecivel toModel(PerecivelDto perecivelDto);

    PerecivelDto toDto(Perecivel perecivel);
}
