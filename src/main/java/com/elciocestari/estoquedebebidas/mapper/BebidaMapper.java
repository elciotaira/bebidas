package com.elciocestari.estoquedebebidas.mapper;

import com.elciocestari.estoquedebebidas.dto.BebidaDto;
import com.elciocestari.estoquedebebidas.entity.Bebida;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface BebidaMapper extends ProdutoMapper<Bebida, BebidaDto>{

    BebidaMapper INSTANCE = Mappers.getMapper( BebidaMapper.class );

    @Mapping(source = "dataDeValidade", target = "validade", dateFormat = "dd.MM.yyyy")
    Bebida toModel(BebidaDto dto);

    @Mapping(source = "validade", target = "dataDeValidade", dateFormat = "dd.MM.yyyy")
    BebidaDto toDto(Bebida model);
}
