package com.elciocestari.estoquedebebidas.mapper;

import com.elciocestari.estoquedebebidas.dto.BebidaDto;
import com.elciocestari.estoquedebebidas.dto.ProdutoDto;
import com.elciocestari.estoquedebebidas.entity.Bebida;
import com.elciocestari.estoquedebebidas.entity.Produto;
import org.mapstruct.Mapper;

//@Mapper(componentModel = "spring")
public interface ProdutoMapper<E, D> {
    E toModel(D dto);
    D toDto(E model);
}
