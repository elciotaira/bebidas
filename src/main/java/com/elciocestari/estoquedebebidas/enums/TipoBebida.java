package com.elciocestari.estoquedebebidas.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoBebida {

    @JsonProperty("alcoolica") ALCOOLICA("alcoolica"),
    @JsonProperty("nao-alcoolica") NAO_ALCOOLICA("não alcoolica");

   private String tipo;
}
