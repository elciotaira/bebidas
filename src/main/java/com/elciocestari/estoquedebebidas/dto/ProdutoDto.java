package com.elciocestari.estoquedebebidas.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Future;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


/*
* Nesse padrão o JSON é
 {
    "bebidaDto" :{
    "nome": "fanta",
    "volume": 100.00
    }
}
* */
//@JsonTypeInfo (
//        use = JsonTypeInfo.Id.NAME,
//        include = JsonTypeInfo.As.PROPERTY,
//        property = "name"
//)
//@JsonSubTypes ({
//        @JsonSubTypes.Type(value = BebidaDto.class, name = "bebidaDto"),
//        @JsonSubTypes.Type(value = PerecivelDto.class, name = "perecivelDto")
//})
/*
* nesse caso um exemplo de JSON é
{
    "name": "perecivelDto",
    "nome": "fanta",
    "volume": 100.00
}
*/
@Getter
@Setter
@JsonTypeInfo (use = JsonTypeInfo.Id.NAME,include = JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonSubTypes ({
        @JsonSubTypes.Type(value = BebidaDto.class),
        @JsonSubTypes.Type(value = PerecivelDto.class)
})
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public abstract class ProdutoDto{

    @NotNull
    private String nome;

    @Min(value = 0, message = "{valor_minimo}")
    private double volume;

    @Min(value = 0)
    private double quantidade;

    @NotEmpty
    private String dataDeValidade;

}
