package com.elciocestari.estoquedebebidas.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Data
@SuperBuilder
@JsonTypeName("BebidaDto")
@Component
@NoArgsConstructor
public class BebidaDto extends ProdutoDto{

    @NotNull
    private String tipoBebida;
//    private String dataDeValidade;
}
