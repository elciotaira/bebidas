package com.elciocestari.estoquedebebidas.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@JsonTypeName("PerecivelDto")
@NoArgsConstructor
@AllArgsConstructor
public class PerecivelDto extends ProdutoDto{
    private double temperadaEmGrausCelcius;
}
//{
//        "PerecivelDto":{
//        "nome": "carne",
//        "volume": 100.00,
//        "quantidade": 15.00
//        }
//        }