package com.elciocestari.estoquedebebidas.dto;

public interface BaseMapper<E,T> {
    E mapDtoToModel();
    T mapModelToDto(E e);
}
