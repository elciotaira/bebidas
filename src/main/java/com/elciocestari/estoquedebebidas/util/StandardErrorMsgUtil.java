package com.elciocestari.estoquedebebidas.util;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
public class StandardErrorMsgUtil implements Serializable {
    private static final long serialVersionUID = 1L;
    private Date timestamp;
    private Integer status;
    private String error;
    private String message;
    private String path;
}
