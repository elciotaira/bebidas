package com.elciocestari.estoquedebebidas.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatUtil {

    /**
     * @param date
     * @return String formatada em dd.mm.yyyy
     */
    public static String formatDateToStringInddMMyyyWithMask(Date date) {
        SimpleDateFormat formato = new SimpleDateFormat("dd.MM.yyyy");
        String data = formato.format(date);
        return data;
    }

    /**
     * @param date
     * @param pattner - O padrão no qual será formatado a String.
     *                Ex: "dd/MM/yyyy" "dd.MM.yyyy"
     *                Entre outros. @{@link SimpleDateFormat }
     * @return String formatada no padrão fornecido em pattner
     */
    public static String formatDateToStringInPattern(Date date, String pattner) {
        if (date == null || pattner == null) {
            throw new IllegalArgumentException("date or pattner must not be null");
        }
        SimpleDateFormat formato = new SimpleDateFormat(pattner);
        String data = formato.format(date);
        return data;
    }

    /**
     * @param date
     * @return Date com a String date formata ou null caso ocorra alguma exceção.
     */
    public static Date formatStringToDate(String date) {
        if (date == null) {
            throw new IllegalArgumentException("data can't be null");
        }
        SimpleDateFormat formato = new SimpleDateFormat("dd.MM.yyyy");
        Date data;
        try {
            data = formato.parse(date);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Not be able to parse ");
        }

        return data;
    }
}
