package com.elciocestari.estoquedebebidas.controller.exceptionhandler;

import com.elciocestari.estoquedebebidas.exception.ResourceNotFoundException;
import com.elciocestari.estoquedebebidas.util.StandardErrorMsgUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class HandlerException {

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<StandardErrorMsgUtil> jsonParseException(HttpMessageNotReadableException exception, HttpServletRequest request) {
        StandardErrorMsgUtil errorMsg = StandardErrorMsgUtil.builder()
                .error("Erro ao converter o JSON para o objeto")
                .message(exception.getMessage())
                .path(request.getRequestURI())
                .timestamp(new Date())
                .status(HttpStatus.BAD_REQUEST.value())
                .build();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMsg);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<StandardErrorMsgUtil> resourceNotFoundException(ResourceNotFoundException exception, HttpServletRequest request) {
        StandardErrorMsgUtil errorMsg = StandardErrorMsgUtil.builder()
                .error("Recurso não localizado.")
                .message(exception.getMessage())
                .path(request.getRequestURI())
                .timestamp(new Date())
                .status(HttpStatus.NOT_FOUND.value())
                .build();

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMsg);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<StandardErrorMsgUtil> methodArgumentNotValidException(MethodArgumentNotValidException ex, HttpServletRequest request) {
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(fieldError -> fieldError.getField() + " " + fieldError.getDefaultMessage())
                .collect(Collectors.toList());

        StandardErrorMsgUtil errorMsgUtil = StandardErrorMsgUtil.builder()
                .error("Entrada inválida")
                .path(request.getRequestURI())
                .message(errors.toString())
                .timestamp(new Date())
                .status(HttpStatus.BAD_REQUEST.value()).build();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMsgUtil);
    }

}
