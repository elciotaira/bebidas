package com.elciocestari.estoquedebebidas.controller;

import com.elciocestari.estoquedebebidas.dto.ProdutoDto;
import com.elciocestari.estoquedebebidas.service.ProdutoService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @GetMapping
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok( this.produtoService.getAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<?> findOne(@PathVariable("id") Long id) throws NotFoundException {
        return ResponseEntity.ok( this.produtoService.findById(id));
    }

    @PostMapping
    public ResponseEntity<ProdutoDto> save(@RequestBody @Valid ProdutoDto produtoDto){
        return ResponseEntity.ok(this.produtoService.save(produtoDto));
    }

    @PutMapping("{id}")
    public ResponseEntity<?> update(@RequestBody @Valid ProdutoDto produtoDto, @PathVariable("id") Long id){
        return ResponseEntity.ok(this.produtoService.update(id, produtoDto));
    }

}
