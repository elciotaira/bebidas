package com.elciocestari.estoquedebebidas;

import lombok.extern.java.Log;
import org.junit.jupiter.api.Test;

import java.util.*;

@Log
class EstoquedebebidasApplicationTests {

	private static Map<Integer, Integer[][]> hashMap = new HashMap<>();

	@Test
	void contextLoads() {
	}

	@Test
	public void testeOrdenacao() {
		List<Integer> list = Arrays.asList(1,2,36,0,10,4,5, 3);
//		Collections.sort(list, new Comparator<Integer>(){
//			@Override
//			public int compare(Integer o1, Integer o2) {
//				return o1-o2;
//			}
//		});

		Collections.sort(list, ((o1, o2) -> {
			Integer[][] dados = new Integer[1][2];
			dados[0][0] = o1; dados[0][1] = o2;
			hashMap.put((o1-o2), dados);
			return o1-o2;
		}));

		log.info(String.valueOf(list));
	}

}
