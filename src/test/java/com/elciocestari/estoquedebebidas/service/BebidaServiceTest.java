package com.elciocestari.estoquedebebidas.service;

import com.elciocestari.estoquedebebidas.entity.Bebida;
import com.elciocestari.estoquedebebidas.repository.BebidaRepository;
import com.elciocestari.estoquedebebidas.util.MockBebidaUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class BebidaServiceTest {

    @Mock
    private BebidaRepository bebidaRepository;

    @Test
    public void getAll_whenThereIsData(){
        when(bebidaRepository.findAll()).thenReturn(MockBebidaUtil.getBebidasList(10));

        List<Bebida> bebidaList = this.bebidaRepository.findAll();

        assertThat(bebidaList).isNotNull();
        assertThat(bebidaList.size()).isEqualTo(10);
    }

}