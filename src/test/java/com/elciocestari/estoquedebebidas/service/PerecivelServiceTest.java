package com.elciocestari.estoquedebebidas.service;


import com.elciocestari.estoquedebebidas.dto.PerecivelDto;
import com.elciocestari.estoquedebebidas.entity.Perecivel;
import com.elciocestari.estoquedebebidas.mapper.PerecivelMapper;
import com.elciocestari.estoquedebebidas.repository.PerecivelRepository;
import com.elciocestari.estoquedebebidas.util.MockPerecivelUtil;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.elciocestari.estoquedebebidas.util.MockPerecivelUtil.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class PerecivelServiceTest {

    @Mock private PerecivelRepository perecivelRepository;
    @Mock private PerecivelMapper perecivelMapper;
    @InjectMocks private PerecivelService perecivelService;

    @Before
    public void setUp() {
        when(perecivelMapper.toModel(any(PerecivelDto.class))).thenReturn(getPerecivel());
        when(perecivelMapper.toDto(any(Perecivel.class))).thenReturn(getPerecivelDto());
        when(perecivelRepository.update(any(Perecivel.class), anyLong())).thenReturn(1);
        when(perecivelRepository.save(any(Perecivel.class))).thenReturn(getPerecivel());
    }

    @Test
    public void getAll_whenListIsNotEmpty() {
        when(perecivelRepository.findAll()).thenReturn((getPerecivelList(10)));
        List<PerecivelDto> dtoList = this.perecivelService.getAll();
        assertThat(dtoList).isNotNull();
        assertThat(dtoList.size()).isEqualTo(10);
    }

    @Test
    public void getAll_whenListIsEmpty() {
        when(perecivelRepository.findAll()).thenReturn((getPerecivelList(10)));
        List<PerecivelDto> dtoList = this.perecivelService.getAll();
        assertThat(dtoList).isNotNull();
        assertThat(dtoList.size()).isEqualTo(10);
    }

    @Test
    public void findById() {
    }

    @Test
    public void save() {
        PerecivelDto expected = getPerecivelDto();
        PerecivelDto actual = this.perecivelService.save(expected);
        assertThat(actual).isNotNull();
        assertThat(actual.getNome()).isEqualTo(expected.getNome());
        assertThat(actual.getDataDeValidade()).isEqualTo(expected.getDataDeValidade());
        assertThat(actual.getTemperadaEmGrausCelcius()).isEqualTo(expected.getTemperadaEmGrausCelcius());
        assertThat(actual.getQuantidade()).isEqualTo(expected.getQuantidade());
        assertThat(actual.getVolume()).isEqualTo(expected.getVolume());
    }

    @Test
    public void update_whenSuccessFul() {
        PerecivelDto expected = this.perecivelService.update(1L, MockPerecivelUtil.getPerecivelDto());
         assertThat(expected).isNotNull();
    }
}