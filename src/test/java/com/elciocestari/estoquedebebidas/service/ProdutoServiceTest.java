package com.elciocestari.estoquedebebidas.service;


import com.elciocestari.estoquedebebidas.dto.BebidaDto;
import com.elciocestari.estoquedebebidas.dto.PerecivelDto;
import com.elciocestari.estoquedebebidas.dto.ProdutoDto;
import com.elciocestari.estoquedebebidas.exception.ResourceNotFoundException;
import com.elciocestari.estoquedebebidas.mapper.BebidaMapper;
import com.elciocestari.estoquedebebidas.mapper.PerecivelMapper;
import com.elciocestari.estoquedebebidas.repository.BebidaRepository;
import com.elciocestari.estoquedebebidas.repository.ProdutoRepository;
import com.elciocestari.estoquedebebidas.util.MockBebidaUtil;
import com.elciocestari.estoquedebebidas.util.MockPerecivelUtil;
import lombok.extern.java.Log;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static com.elciocestari.estoquedebebidas.util.MockBebidaUtil.*;
import static com.elciocestari.estoquedebebidas.util.MockPerecivelUtil.getPerecivelDto;
import static com.elciocestari.estoquedebebidas.util.MockPerecivelUtil.getPerecivelDtoList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@Log
@RunWith(SpringRunner.class)
public class ProdutoServiceTest {
    @InjectMocks private ProdutoService produtoService;
    @Mock private BebidaService bebidaService;
    @Mock private PerecivelService perecivelService;
    @Mock private ProdutoRepository produtoRepository;
    @Mock private BebidaRepository bebidaRepository;
    @Mock private BebidaMapper bebidaMapper;
    @Mock private PerecivelMapper perecivelMapper;

    @Before
    public void setup() {
        when(bebidaService.getAll()).thenReturn(getBebidasDtoList(10));
        when(perecivelService.getAll()).thenReturn(getPerecivelDtoList(10));
        when(perecivelService.save(any(PerecivelDto.class))).thenReturn(getPerecivelDto());
        when(bebidaService.save(any(BebidaDto.class))).thenReturn(getBebidaDto());
    }

    @Test
    public void getAll_whenThereIsDataOnEntities() {
        List<ProdutoDto> produtoList = this.produtoService.getAll();
        assertThat(produtoList).isNotNull();
        assertThat(produtoList.size()).isGreaterThan(0);
        assertThat(produtoList.size()).isEqualTo(20);
    }

    @Test
    public void findById_whenPerecivelFound_thenSuccessful() {
        when(bebidaService.findById(anyLong())).thenReturn(null);
        when(perecivelService.findById(anyLong())).thenReturn(getPerecivelDto());
        ProdutoDto produtoDtoExpected = this.produtoService.findById(1L);
        assertThat(produtoDtoExpected).isNotNull();
    }

    @Test
    public void findById_whenBebidaFound_thenSuccessful() {
//        Long random = new Random().nextLong();
        Long random = ThreadLocalRandom.current().nextLong(1000L);
        BebidaDto bebidaDto = getBebidaDto();
        when(bebidaService.findById(anyLong())).thenReturn(bebidaDto);
        when(perecivelService.findById(anyLong())).thenReturn(null);
        ProdutoDto produtoDtoExpected = this.produtoService.findById(random);
        assertThat(produtoDtoExpected).isNotNull();
        assertThat(produtoDtoExpected).isEqualTo(bebidaDto);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void findById_whenNotFound_thenThrowException() {
        when(bebidaService.findById(anyLong())).thenReturn(null);
        when(perecivelService.findById(anyLong())).thenReturn(null);
        ProdutoDto produtoDtoExpected = this.produtoService.findById(1L);
    }

    @Test
    public void update() {
    }

    @Test
    public void save_whenPerecivel_thenSuccessful() {
        ProdutoDto expected = getPerecivelDto();
        ProdutoDto actual = this.produtoService.save(expected);
        assertThat(actual).isEqualTo(expected);
        assertThat(actual).isInstanceOf(PerecivelDto.class);
    }

    @Test
    public void save_whenBebida_thenSuccessful() {
        ProdutoDto expected = getBebidaDto();
        ProdutoDto actual = this.produtoService.save(expected);
        assertThat(actual).isEqualTo(expected);
        assertThat(actual).isInstanceOf(BebidaDto.class);
    }
}