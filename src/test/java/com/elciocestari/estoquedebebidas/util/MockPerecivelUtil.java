package com.elciocestari.estoquedebebidas.util;

import com.elciocestari.estoquedebebidas.dto.PerecivelDto;
import com.elciocestari.estoquedebebidas.entity.Perecivel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MockPerecivelUtil {

    public static Perecivel getPerecivel() {
        Perecivel perecivel = Perecivel.builder()
                .nome("Mortadela")
                .volume(200.00)
                .quantidade(1)
                .temperadaEmGrausCelcius(10.00)
                .validade(new Date())
                .build();
        return perecivel;
    }

    public static List<Perecivel> getPerecivelList(int quantity) {
        List<Perecivel> list = new ArrayList<>();
        while (quantity > 0) {
            list.add(getPerecivel());
            quantity--;
        }
        return list;
    }

    public static List<PerecivelDto> getPerecivelDtoList(int quantity) {
        List<PerecivelDto> list = new ArrayList<>();
        while (quantity > 0) {
            list.add(getPerecivelDto());
            quantity--;
        }
        return list;
    }

    public static PerecivelDto getPerecivelDto() {
        return PerecivelDto.builder()
                .nome("Mortadela")
                .volume(200.00)
                .quantidade(1)
                .temperadaEmGrausCelcius(10.00)
                .dataDeValidade(new Date().toString())
                .build();
    }
}