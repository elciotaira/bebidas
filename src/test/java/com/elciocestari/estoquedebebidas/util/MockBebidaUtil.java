package com.elciocestari.estoquedebebidas.util;

import com.elciocestari.estoquedebebidas.dto.BebidaDto;
import com.elciocestari.estoquedebebidas.entity.Bebida;
import com.elciocestari.estoquedebebidas.enums.TipoBebida;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MockBebidaUtil {

    public static Bebida getBebida() {
        Bebida bebida = Bebida.builder()
                .volume(600.00)
                .nome("Fanta")
                .validade(new Date())
                .tipoBebida(TipoBebida.NAO_ALCOOLICA)
                .build();
        return bebida;
    }

    public static List<Bebida> getBebidasList(int quantity) {
        List<Bebida> list = new ArrayList<>();
        while (quantity > 0) {
            list.add(getBebida());
            quantity--;
        }
        return list;
    }

    public static List<BebidaDto> getBebidasDtoList(int quantity) {
        List<BebidaDto> list = new ArrayList<>();
        while (quantity > 0) {
            list.add(getBebidaDto());
            quantity--;
        }
        return list;
    }

    public static BebidaDto getBebidaDto() {
        return BebidaDto.builder()
                .volume(600.00)
                .nome("Fanta")
                .dataDeValidade(new Date().toString())
                .tipoBebida(TipoBebida.NAO_ALCOOLICA.toString())
                .build();
    }
}