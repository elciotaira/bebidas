package com.elciocestari.estoquedebebidas.util;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.MatchesPattern.matchesPattern;

public class DateFormatUtilTest {

    @Test
    public void whenSendADate_assertThatIsAceptFormat() {
        Date date = new Date();

        String dateFormataded = DateFormatUtil.formatDateToStringInddMMyyyWithMask(date);

        assertThat(dateFormataded, CoreMatchers.notNullValue());
        assertThat(dateFormataded, matchesPattern("([0-9]{2}['.']{1}[0-9]{2}['.']{1}[0-9]{4})"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void formatDateToStringInPattern_whenSendNullThrowException(){
        DateFormatUtil.formatDateToStringInPattern(null, null);
    }
}
