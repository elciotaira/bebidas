package com.elciocestari.estoquedebebidas.controller;

import com.elciocestari.estoquedebebidas.dto.BebidaDto;
import com.elciocestari.estoquedebebidas.dto.PerecivelDto;
import com.elciocestari.estoquedebebidas.dto.ProdutoDto;
import com.elciocestari.estoquedebebidas.exception.ResourceNotFoundException;
import com.elciocestari.estoquedebebidas.service.ProdutoService;
import com.elciocestari.estoquedebebidas.util.MockBebidaUtil;
import com.elciocestari.estoquedebebidas.util.MockPerecivelUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.java.Log;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Log
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ProdutoController.class)
public class ProdutoControllerTest {

    private static List<ProdutoDto> produtoDtoList;
    private static String jsonList;
    private static BebidaDto bebidaDto;
    private static PerecivelDto perecivelDto;
    private static String jsonBebidaDto;
    private static String jsonPerecivelDto;
    private String bebidaDtoOptional;

    private ProdutoDto produtoDto;
    private ObjectMapper objectMapper;

    @Autowired private MockMvc mockMvc;
    @MockBean private ProdutoService produtoService;

    @Before
    public void setup() throws JsonProcessingException {
        bebidaDto = MockBebidaUtil.getBebidaDto();
        perecivelDto = MockPerecivelUtil.getPerecivelDto();
        objectMapper = new ObjectMapper();
        produtoDtoList = Arrays.asList(bebidaDto);
        jsonList = objectMapper.writeValueAsString(produtoDtoList);
        jsonBebidaDto = objectMapper.writeValueAsString(bebidaDto);
        jsonPerecivelDto = objectMapper.writeValueAsString(perecivelDto);
        bebidaDtoOptional = objectMapper.writeValueAsString(Optional.ofNullable(bebidaDto));

        produtoDto = this.bebidaDto;
    }

    @Test
    public void whenGet_responseProdutoDtoList() throws Exception {
        when(this.produtoService.getAll()).thenReturn(produtoDtoList);

        mockMvc.perform(get("/produtos"))
                .andExpect(status().isOk())
                .andExpect(content().json(jsonList));
    }

    //TODO esse teste esta apresentando erro, porem nao era pra apresentar. Estudar e resolver o caso.
    @Test
    public void findById_whenFindThenReturnAProdutoDto() throws Exception {
        when(produtoService.findById(Long.parseLong("1"))).thenReturn(bebidaDto);

        mockMvc.perform(get("/produtos/1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().json(jsonBebidaDto));
    }

    @Test
    public void findById_whenNotFoundThenTrhowException() throws Exception {
//        when(produtoService.findById(Long.parseLong("2"))).thenThrow(new NotFoundException("Não foi encontrado um produto com id [ 2 ]"));
        when(produtoService.findById(Long.parseLong("2"))).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(get("/produtos/2"))
//                    .andExpect(content().string("Não foi encontrado um produto com id [ 2 ]"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void save_whenSuccessful() throws Exception {
        when(this.produtoService.save(any(BebidaDto.class))).thenReturn(bebidaDto);
        mockMvc.perform(post("/produtos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBebidaDto))
                .andExpect(status().isOk())
                .andReturn();
    }
    @Test
    public void save_whenSendPerecivelDto_thenSuccessful() throws Exception {
        when(this.produtoService.save(perecivelDto))
                .thenReturn(perecivelDto);

        mockMvc.perform(post("/produtos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonPerecivelDto))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void save_whenRequestWithInvalidJsonThenCatchEx_thenFail() throws Exception {
        when(this.produtoService.save(any(ProdutoDto.class))).thenReturn(bebidaDto);

        String jsonInvalido = "{\n" +
                "    \"BananaDto\": {\n" +
                "        \"nome\": \"banana\",\n" +
                "        \"valor\": 5.55,\n" +
                "        \"volume\": 2\n" +
                "    }\n" +
                "}";

        mockMvc.perform(post("/produtos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonInvalido))
                .andExpect( result -> {
                    assertThat(result.getResolvedException())
                            .isInstanceOf(HttpMessageNotReadableException.class);
                })
                .andExpect(result -> {
                    assertThat(result.getResponse().getContentAsString())
                            .contains("Erro ao converter o JSON para o objeto");
                })
                .andExpect(result -> {
                    assertThat(result.getResponse().getContentAsString().substring(
                            result.getResponse().getContentAsString().indexOf("error") )
                            .contains("Erro ao converter o JSON para o objeto")).isTrue();
                })
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    public void testJsonConversion() throws JsonProcessingException {
        String json = "{\"BebidaDto\":{\"nome\":\"brhama\",\"volume\":0.0,\"quantidade\":0.0,\"dataDeValidade\":\"04/08/1986\",\"tipoBebida\":\"alcolica\"}}";
        ObjectMapper objectMapper = new ObjectMapper();
        ProdutoDto bebidaDto =  objectMapper.readValue(json, BebidaDto.class);

        assertThat(bebidaDto).isNotNull();
        assertThat(bebidaDto.getNome()).isEqualTo("brhama");
        assertThat(bebidaDto.getDataDeValidade()).isEqualTo("04/08/1986");
    }

    @Test
    public void update_whenBebida_thenSuccessful() throws Exception {
        mockMvc.perform(put("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBebidaDto))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void update_whenInvlidBebida_thenFail() throws Exception {
        int invalidVolume = -10;
        int invalidQuantidade = -1;
        BebidaDto bebidaDto = MockBebidaUtil.getBebidaDto();
        bebidaDto.setVolume(invalidVolume);
        bebidaDto.setQuantidade(invalidQuantidade);

        String invalidJson = this.objectMapper.writeValueAsString(bebidaDto);
        log.info(invalidJson);

        mockMvc.perform(put("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(invalidJson))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", containsStringIgnoringCase("volume Valor minimo inválido")))
                .andReturn();
    }
}
