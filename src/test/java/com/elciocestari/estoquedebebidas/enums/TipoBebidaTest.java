package com.elciocestari.estoquedebebidas.enums;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TipoBebidaTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void jsonConverter_whenLowerCase_thenSuccessful() throws JsonProcessingException {
        TipoBebida alcoolica = objectMapper.convertValue("alcoolica", TipoBebida.class);
        TipoBebida naoAlcoolica = objectMapper.convertValue("nao-alcoolica", TipoBebida.class);

        assertThat(alcoolica).isInstanceOf(TipoBebida.class);
        assertThat(alcoolica).isNotNull();
        assertThat(alcoolica.name()).isEqualTo("ALCOOLICA");
        assertThat(alcoolica.ordinal()).isEqualTo(0);

        assertThat(naoAlcoolica).isInstanceOf(TipoBebida.class);
        assertThat(naoAlcoolica).isNotNull();
        assertThat(naoAlcoolica.name()).isEqualTo("NAO_ALCOOLICA");
        assertThat(naoAlcoolica.ordinal()).isEqualTo(1);

    }
    @Test
    void jsonConverter_whenUperCase_thenSuccessful() throws JsonProcessingException {
        TipoBebida alcoolica = objectMapper.convertValue("ALCOOLICA", TipoBebida.class);
        TipoBebida naoAlcoolica = objectMapper.convertValue("NAO_ALCOOLICA", TipoBebida.class);

        assertThat(alcoolica).isInstanceOf(TipoBebida.class);
        assertThat(alcoolica).isNotNull();
        assertThat(alcoolica.name()).isEqualTo("ALCOOLICA");
        assertThat(alcoolica.ordinal()).isEqualTo(0);

        assertThat(naoAlcoolica).isInstanceOf(TipoBebida.class);
        assertThat(naoAlcoolica).isNotNull();
        assertThat(naoAlcoolica.name()).isEqualTo("NAO_ALCOOLICA");
        assertThat(naoAlcoolica.ordinal()).isEqualTo(1);

    }

    @Test
    void jsonConverter_whenFail() {
        Exception thrown = assertThrows(
                IllegalArgumentException.class,
                () ->{TipoBebida alcoolica = objectMapper.convertValue("alcool", TipoBebida.class);});

        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
    }
}