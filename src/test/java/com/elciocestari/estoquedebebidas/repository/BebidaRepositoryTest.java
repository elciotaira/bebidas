package com.elciocestari.estoquedebebidas.repository;

import com.elciocestari.estoquedebebidas.entity.Bebida;
import com.elciocestari.estoquedebebidas.enums.TipoBebida;
import com.elciocestari.estoquedebebidas.util.MockBebidaUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@DataJpaTest
@DisplayName("BebidaRepostory Test")
class BebidaRepositoryTest {

    @Autowired
    private BebidaRepository bebidaRepository;
    private Bebida bebidaMock;

    @BeforeEach
    public void setup() {
        this.bebidaMock = this.getBebidaMock();
    }

    @Test
    public void findAll_whenTableIsEmpty() {
        List<Bebida> list = this.bebidaRepository.findAll();
        assertThat(list).isNotNull();
        assertThat(list.isEmpty()).isTrue();
    }

    @Test
    public void findAll_whenTableIsNotEmpty() {
        List<Bebida> listToSave = MockBebidaUtil.getBebidasList(10);
        this.bebidaRepository.saveAllAndFlush(listToSave);
        List<Bebida> list = this.bebidaRepository.findAll();
        assertThat(list).isNotNull();
        assertThat(list.size()).isEqualTo(10);
    }

    @Test
    public void save_persistBebida_whenSuccessful() {
//        Bebida bebidaExpected = this.bebidaRepository.save(bebidaMock);
//        assertEquals(bebidaExpected.getNome(), bebidaMock.getNome());
//        assertThat(bebidaExpected).isNotNull();
//        assertThat(bebidaExpected.getId()).isNotNull();
//        assertThat(bebidaExpected.getValidade()).isEqualTo(bebidaMock.getValidade());

        this.bebidaRepository.save(bebidaMock);
        assertThat(bebidaMock.getId()).isNotNull();
    }

    @Test
    public void update_whenSuccessful() {
        String actualName = "Fanta";
        String afterName = "Sprit";

        //antes do update
        Bebida bebida = this.getBebidaMock();
        bebida.setNome(actualName);
        this.bebidaRepository.save(bebida);
        assertThat(bebida.getNome()).isEqualTo(actualName);

        //depois do update
        bebida.setNome(afterName);
        this.bebidaRepository.save(bebida);
        assertThat(bebida.getId()).isNotNull();
        assertThat(bebida.getNome()).isEqualTo(afterName);
    }

    @Test
    public void save_persistBebida_whenFail() {
        Bebida bebidaSaved = null;
        try {
            bebidaSaved = this.bebidaRepository.save(null);
            fail();
        } catch (Exception e) {
            assertThat(bebidaSaved).isNull();
        }
    }

    @Test
    public void delete_whenSuccesful() {
        Bebida bebida = this.getBebidaMock();
        this.bebidaRepository.save(bebida);

        Optional<Bebida> actual = this.bebidaRepository.findById(bebida.getId());
        assertTrue(actual.isPresent());

        this.bebidaRepository.delete(bebida);

        Optional<Bebida> expected = this.bebidaRepository.findById(bebida.getId());
        assertTrue(expected.isEmpty());
    }

    @Test
    public void update_newWay_whenSuccessful() {
        Bebida bebida = this.bebidaRepository.save(bebidaMock);
        int i = this.update(bebida);

        Optional<Bebida> bebidaActual = this.bebidaRepository.findById(bebida.getId());

        assertThat(bebidaActual).isNotNull();
        assertThat(bebidaActual.get()).isNotNull();
        assertThat(i).isGreaterThan(0);
        assertThat(bebidaActual.get().getNome()).isEqualTo("FANTA");

    }

    private int update(Bebida bebida) {
        return this.bebidaRepository.update(
                TipoBebida.NAO_ALCOOLICA,
                "FANTA",
                600.00,
                2,
                new Date(),
                bebida.getId());
    }

    private Bebida save(Bebida bebidaMock) {
        return this.bebidaRepository.save(bebidaMock);
    }

    private Bebida getBebidaMock() {
        return Bebida.builder()
                .nome("Coca-cola")
                .tipoBebida(TipoBebida.NAO_ALCOOLICA)
                .validade(new Date())
                .volume(600.00)
                .build();
    }

    @Test
    void Update_wihOnlyEntity_whenSuccessful() {
        String outraCoisaQualquer = "OUTRA COISA QUALQUER";

        Bebida bebida = this.getBebidaMock();
        this.save(bebida);
        this.bebidaRepository.flush();
        assertThat(bebida.getId()).isNotNull();
        assertThat(bebida.getNome()).isNotEqualTo(outraCoisaQualquer);

        bebida.setNome(outraCoisaQualquer);
        int quantity = this.bebidaRepository.update(bebida, bebida.getId());
        assertThat(quantity).isGreaterThan(0);
        assertThat(bebida.getNome()).isEqualTo(outraCoisaQualquer);
    }
}