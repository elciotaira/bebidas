package com.elciocestari.estoquedebebidas.repository;

import com.elciocestari.estoquedebebidas.entity.Perecivel;
import com.elciocestari.estoquedebebidas.util.MockPerecivelUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class PerecivelRepositoryTest {

    @Autowired private PerecivelRepository perecivelRepository;

    @Test
    void update() {
        String nomeQualqer = "Outro nome qualqer";

        Perecivel perecivelExpected = MockPerecivelUtil.getPerecivel();
        Perecivel perecivelActual = this.perecivelRepository.saveAndFlush(perecivelExpected);
        assertThat(perecivelActual).isNotNull();
        assertThat(perecivelActual.getNome()).isNotEqualTo(nomeQualqer);

        perecivelActual.setNome(nomeQualqer);
        int updated = this.perecivelRepository.update(perecivelActual, perecivelActual.getId());
        this.perecivelRepository.flush();
        assertThat(updated).isGreaterThan(0);
        assertThat(perecivelExpected.getNome()).isEqualTo(nomeQualqer);
    }
}