package com.elciocestari.estoquedebebidas.mapper;

import com.elciocestari.estoquedebebidas.dto.BebidaDto;
import com.elciocestari.estoquedebebidas.entity.Bebida;
import com.elciocestari.estoquedebebidas.enums.TipoBebida;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import static com.elciocestari.estoquedebebidas.util.DateFormatUtil.formatDateToStringInddMMyyyWithMask;
import static com.elciocestari.estoquedebebidas.util.MockBebidaUtil.getBebida;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class BebidaMapperTest {

    private ObjectMapper objectMapper;


    @Rule
    public ErrorCollector error = new ErrorCollector();
    private BebidaMapper bebidaMapper = BebidaMapper.INSTANCE;

    @Before
    public void setup(){
        objectMapper = new ObjectMapper();
    }

    @Test
    public void whenBebidaDtoWasMappedToBebida_assertThatAllAttributesAreEquals() throws ParseException {

        BebidaDto bebidaDto = BebidaDto.builder()
                .nome("Coca-Cola")
                .tipoBebida("ALCOOLICA")
                .dataDeValidade("11.07.2022")
                .volume(2000)
                .quantidade(2)
                .build();

        Bebida bebida = bebidaMapper.toModel(bebidaDto);

        SimpleDateFormat formato = new SimpleDateFormat("dd.MM.yyyy");
        Date data = formato.parse(bebidaDto.getDataDeValidade());

        // TipoBebida é um enum, por isso utilizei o toString,
        // eles são de tipos diferentes, porém seu conteudo é igual
        error.checkThat(bebida.getTipoBebida().toString(), CoreMatchers.equalTo(bebidaDto.getTipoBebida()));
        error.checkThat(bebida.getValidade(), CoreMatchers.equalTo(data));
        error.checkThat(bebida.getNome(), CoreMatchers.equalTo(bebidaDto.getNome()));
        error.checkThat(bebida.getVolume(), CoreMatchers.equalTo(bebidaDto.getVolume()));
        error.checkThat(bebida.getQuantidade(), CoreMatchers.equalTo(bebidaDto.getQuantidade()));
    }
    @Test
    public void whenBebidaWasMappedToBebidaDto_assertThatAllAttributesAreEquals(){

        Bebida bebida = getBebida();

        BebidaDto bebidaDto = bebidaMapper.toDto(bebida);

        // TipoBebida é um enum, por isso utilizei o toString,
        // eles são de tipos diferentes, porém seu conteudo é igual
        assertThat(bebida.getTipoBebida().toString(), equalTo(bebidaDto.getTipoBebida()));
        assertThat(formatDateToStringInddMMyyyWithMask(bebida.getValidade()), equalTo(bebidaDto.getDataDeValidade()));
        assertThat(bebida.getNome(), equalTo(bebidaDto.getNome()));
    }
}
