package com.elciocestari.estoquedebebidas.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import com.fasterxml.jackson.databind.jsontype.PolymorphicTypeValidator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;


public class ProdutoDtoTest {

    @Rule
    public ErrorCollector error = new ErrorCollector();
    private ObjectMapper objectMapper;

    @Before
    public void setup(){
        //prepara a montagem do JSON
        PolymorphicTypeValidator ptv = BasicPolymorphicTypeValidator.builder().build();
        objectMapper = new ObjectMapper();
        objectMapper.activateDefaultTyping(ptv);
    }

    @Test
    public void whenSendBebidaDtoOrPerecivelDto_assertThatIsAnInstanceOfProdutoDtoAndOfEntity()
            throws JsonProcessingException {

        ProdutoDto bebida = BebidaDto.builder()
                .nome("fanta")//atributo de super classe ProdutoDto
                .tipoBebida("alcoolica")//atributo da classe especifica BebidaDto
                .build();

        ProdutoDto perecivel = PerecivelDto.builder()
                .nome("mortadela")//atributo da super classe ProdutoDto
                .temperadaEmGrausCelcius(10.0)//atributo da classe especifica PerecívellDto
                .build();

        String jsonBebida = objectMapper.writeValueAsString(bebida);
        String jsonPerecivel = objectMapper.writeValueAsString(perecivel);

        BebidaDto bebidaDto = objectMapper.readValue(jsonBebida, BebidaDto.class);
        PerecivelDto perecivelDto = objectMapper.readValue(jsonPerecivel, PerecivelDto.class);

        error.checkThat(bebidaDto, instanceOf(ProdutoDto.class));
        error.checkThat(bebidaDto, instanceOf(BebidaDto.class));
        error.checkThat(perecivelDto, instanceOf(ProdutoDto.class));
        error.checkThat(perecivelDto, instanceOf(ProdutoDto.class));
    }
}
